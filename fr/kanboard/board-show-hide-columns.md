Afficher ou cacher des colonnes dans le tableau
===============================================

Vous pouvez très facilement cacher ou afficher des colonnes dans le tableau :

![Cacher une colonne](screenshots/hide-column.png)

Pour cacher une colonne, ouvrez le menu déroulant de la colonne.

![Afficher une colonne](screenshots/show-column.png)

Pour afficher de nouveau la colonne, cliquez sur l'icône avec le « plus ».

## Supprimer une colonne

Pour supprimer une colonne vous devez être administrateur du projet puis&nbsp;:

  * cliquer sur **Menu** > **Préférences**
  * cliquer sur **Colonnes** dans le menu latéral gauche
  * cliquer sur l'icône *roue crantée* à côté de la colonne à supprimer
  * cliquer sur **Supprimer** et confirmer votre choix

![image suppression colonne](screenshots/board_suppression_colonne.png)

<p class="alert alert-warning">Il n'est pas possible de supprimer une colonne composée de tâches (qu'elles soient ouvertes ou fermées) : vous devez les <b>supprimer</b> pour povoir supprimer la colonne.</p>
