# Déframasoftiser Framaboard

Afin de [déframasoftiser Internet](https://framablog.org/2019/09/24/deframasoftisons-internet/) voici comment exporter vos données depuis nos services.

## Exporter

Pour exporter les données de votre espace Framaboard vous devez&nbsp;:

  1. vous rendre dans vos préférences (en cliquant sur votre avatar en haut à droite
  * cliquer sur **Préférences**
  * cliquer sur **Télécharger la base de données (Fichier Sqlite compressé en Gzip)** dans la section **Base de données**
  * enregistrer le fichier sur votre ordinateur

![Image export données](screenshots/kanboard-export-donnees.png)

## Importer

Pour importer une base de données précédemment télécharger (voir [ci-dessus](#exporter)), vous devez&nbsp;:

  1. vous rendre dans vos préférences (en cliquant sur votre avatar en haut à droite
  * cliquer sur **Préférences**
  * cliquer sur **Téléverser la base de données** dans la section **Base de données**
  * cliquer sur **Browse…** pour récupérer le fichier exporté
  * cliquer sur **Téléverser**

## Supprimer son compte Framaboard

Pour supprimer votre espace Framaboard, vous devez en être **administrateur/administratrice**, puis&nbsp;:

  1. cliquer sur <i class="fa fa-caret-down" aria-hidden="true"></i> tout en haut à droite de l’espace
  1. cliquer sur **<i class="fa fa-cog" aria-hidden="true"></i> Préférences**
  1. cliquer sur **Gestion espace Framaboard** dans le menu latéral gauche
  1. entrer votre mot de passe dans le champ correspondant
  1. cliquer sur **Supprimer mon espace**

**ATTENTION**&nbsp;: toute suppression est définitive. Nous ne restaurerons aucun espace.

Pour supprimer des utilisateurs/utilisatrices : [voir notre documentation](https://docs.framasoft.org/fr/kanboard/user-management.html#supprimer-des-utilisateurs).
