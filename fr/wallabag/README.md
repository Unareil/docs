# Framabag

<div class="alert-warning alert">
<b>Déframasoftisons Internet</b> : l’association Framasoft a programmé la fermeture de certains services, pour éviter d’épuiser nos maigres ressources et pour favoriser l’utilisation de petits hébergeurs de proximité (<a href="https://framablog.org/2020/03/03/10-bonnes-raisons-de-fermer-certains-services-framasoft-la-5e-est-un-peu-bizarre/">toutes les infos ici</a>).
<b class="violet">Frama</b><b class="vert">bag</b> fermera ses portes le <b>mardi 6 juillet 2021</b>.
Nous avons réuni des outils pour vous aider à récupérer vos données et à trouver un service similaire <a href="https://alt.framasoft.org/fr/framabag">sur cette page</a>.
</div>

Framabag vous permet de mettre de côté les articles que vous n’avez pas le temps de lire tout de suite.

Framabag repose sur le logiciel libre [Wallabag](https://wallabag.org/fr)

[![](images/wallabag.png)](https://wallabag.org/fr)

Pour vous expliquer le principe, voici une présentation de Pyves réalisée avec le logiciel RevealJS et les dessins de [Gee](http://ptilouk.net/) sous licence CC-By-SA.

<div class="embed-responsive embed-responsive-16by9">
  <iframe src="https://framabag.org/cquoi/" class="embed-responsive-item" frameborder="0" marginwidth="0" marginheight="0" scrolling="no"></iframe>
</div>

---

La documentation ci-dessous provient du site officiel de [Wallabag](http://doc.wallabag.org/fr/master/).

## Table des matières
  * [Déframasoftiser Internet](deframasoftiser.html)
  * [Se créer un compte](user/create_account.html)
  * [Configuration](user/configuration.html)
  * [Migrer depuis…](user/import.html)
  * [Articles](user/articles.html)
  * [Erreurs durant la récupération des articles](user/errors_during_fetching.html)
  * [Retrouver des articles grâce aux filtres](user/filters.html)
  * [Configurer les applications mobile pour wallabag](user/configuring_mobile.html)
  * [Application Android](user/android.html)
