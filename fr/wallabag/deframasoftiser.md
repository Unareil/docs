# Déframasoftiser Framabag

Afin de [déframasoftiser Internet](https://framablog.org/2019/09/24/deframasoftisons-internet/) voici comment exporter vos données depuis nos services.

<div class="alert-info alert">Framabag sera invisibilisé ou Fermé Mi 2021.</div>

## Exporter

Pour exporter tous vos articles Framabag, vous devez cliquer sur l'icône ![icône de téléchargement des articles](images/download_article.png) depuis la page [Tous les articles](https://framabag.org/all/list).

Si vous ne souhaitez exporter que les articles non lus, c'est à partir de l'onglet [Non lus](https://framabag.org/unread/list) que vous devez cliquer sur l'icône ![icône de téléchargement des articles](images/download_article.png) etc…

Vous pouvez télécharger vos articles dans plusieurs formats : ePUB, MOBI, PDF, XML, JSON, CSV.

## Importer

Pour importer vos articles dans une autre instance Wallabag, vous devez&nbsp;:
 1. cliquer sur **Importer** dans le menu latéral gauche
 * cliquer sur **Importer les contenus** dans la section **wallabag v2**
 * cliquer sur le bouton **Fichier** pour récupérer le fichier d'export ([voir ci-dessus](#exporter))
 * sélectionner votre fichier
 * cliquer sur **Importer le fichier**
