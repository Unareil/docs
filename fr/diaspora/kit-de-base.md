## Diaspora\* : qu'est-ce que c'est ?

Diaspora\* est un réseau social
[libre](https://fr.wikipedia.org/wiki/Logiciel_libre), donc [open
source](https://fr.wikipedia.org/wiki/Open_source) et décentralisé,
c'est-à-dire qu'il tourne sur plusieurs serveurs interconnectés.
Facebook, Twitter ou Google+ stockent **vos données** sur leurs
serveurs. Dans le cas de D\*, vous avez la possibilité de vous inscrire
sur un serveur (appelé **pod**) déjà existant, ou créer le votre pour
une **protection de la vie privée optimale**. Dans le cas d'un pod
existant, vos données sont hébergées chez quelqu'un, certes, mais
beaucoup d'entre vous font confiance à des sociétés qui tirent profit de
vos données sans sourcilier , alors que là, vous devez faire confiance,
la plupart du temps, à des défenseurs de la vie privée, et/ou des
libristes. Quand bien même ils auraient envie de revendre vos données,
ça n'intéresserait pas des masses d'acheteurs (qui préféreront se
tourner vers des sociétés qui brassent des millions d'utilisateurs -
suivez mon regard…).

> N'allez pas croire que Diaspora\* est un repère de geeks. Il y en a
beaucoup, certes, mais les profils se diversifient de plus en plus
(beaucoup de rescapés de Facebook par exemple).

## Rencontrer du monde (ou pas)

Diaspora\* fonctionne de deux façons :

-   En mode **Public** pour potentiellement parler à *tout le monde* (et
    donc visible par tout Internet)
-   En mode **Limité**, c'est à dire juste pour vos contacts

### En public : \#nouveauici

Si vous voulez rencontrer du monde, vous devez mettre votre message en
**public**, mais ce n'est pas tout, vous devez mettre des **tags**. Si
vous mettez un message en **public**, mais sans tags, il sera
principalement vu par **vos contacts** ou si une personne visite votre
profil. Il n'apparaîtra pas dans **le flux** des autres utilisateurs.

Dans le flux des utilisateurs apparaissent :

-   les messages postés par **vos contacts** (publics **ET** privés)
-   les posts de personnes n'appartenant pas obligatoirement à vos
    contacts, mais qui utilisent **un tag que vous suivez**

Lorsqu'on arrive sur Diaspora\*, et qu'on souhaite rencontrer du monde,
deux choses sont primordiales si on ne veut pas faire un *flop* et se
dire qu'il n'y a personne dans le coin :

-   Mettre le post en **public**
-   Mettre le tag **\#nouveauici** (le tag
    \#nouvelleici est moins utilisé ; à vous de changer ça en mettant
    **les deux tags** : \#nouvelleici ET \#nouveauici - ou tout autre tag)

Ce message de présentation sera lu par **vos contacts** et par **les
gens qui suivent le tag \#nouveauici / \#nouvelleici**. Le message
"parfait" :

> Bonjour à tous et toutes, je suis \#nouveauici ! J'aime le \#libre,
> \#diaspora, la \#bière et \#spf ! \#fr \#french

Vous aurez peut-être remarqué que j'ai ajouté les tags \#fr et \#french
: cela permet de toucher les francophones qui se regroupent sous ces
tags.

Si vous ne recevez pas de "j'aime", "repartage" ou de commentaires, vous
aurez le droit de vous plaindre auprès de moi.

### En privé

Rien ne vous oblige à partager avec le reste d'Internet et de
Diaspora\*. Si vous souhaitez simplement partager entre ami(e)s, vous
n'avez qu'à créer un groupe (appelé **aspect**) et ne publier **que**
dans cet aspect (même pas obligé de vous présenter en public).

## Ecrire sur Diaspora\*

Diaspora\* utilise la syntaxe markdown. Ne fuyez pas, c'est pas si
compliqué. Je vous donne ici les principales (selon moi bien sûr) :

### Insérer un lien

### Avec l'éditeur

![gif insérer un lien](images/diaspora_inserer_lien.gif)

### Manuellement

Vous pouvez tout simplement coller le lien ("wow, c'est simple") ou
utiliser la syntaxe pour lui donner un peu de style :

    Tiens, va voir [ce lien](http://adresse-du-lien.truc) !

-   entre crochets **[]** : le nom qui sera affiché
-   entre **parenthèses** : l'adresse du lien Pour insérer une vidéo,
    vous n'avez qu'à mettre le lien, et la vidéo s'affichera (après un
    rafraîchissement de la page de votre part).

### Insérer une image

Si c'est une image qui est sur votre ordinateur, il suffit d'appuyer sur
le bouton en forme d'appareil photo dans zone de publication. Si cette
photo vient d'Internet, la syntaxe ressemble à celle plus haut. Cliquez
droit sur l'image voulue, puis sur **copier l'adresse de l'image**, puis
:

### Avec l'éditeur

![gif insérer une image](images/diaspora_inserer_image.gif)

### Manuellement

    ![mon img](http://un-mignon-petit-chat.jpg)

-   un **!** pour dire que c'est une image
-   un texte alternatif (si l'image ne s'affiche pas / plus) entre
    crochets **[]**
-   l'adresse entre **parenthèses** (http://un-mignon-petit-chat.jpg)

## En apprendre plus et participer

Pour en savoir plus, vous avez :

-   le [wiki de Diaspora\*](https://wiki.diasporafoundation.org/)
-   le [guide du parfait
    débutant](https://fr.wikibooks.org/wiki/Diaspora_%3a_Le_guide_du_parfait_d%C3%A9butant)
    (plus complet que ce kit de base)
-   un article pour [comprendre le système de fédération de
    Diaspora\*](http://geexxx.fr/2013/07/23/comment-ca-marche-la-federation-de-diaspora/)

Pour participer :

-   sur le [Github de Diaspora\*](https://github.com/diaspora/diaspora)
-   sur [Loomio](https://www.loomio.org/g/EseV9p4X/diaspora-community)
    pour des discussions sur le projet Pour rejoindre Diaspora\* :
-   Le [pod de framasoft](https://framasphere.org/) (stable)
-   une [liste de "tous" les pods](https://the-federation.info/)
