Prise en main très détaillée
================================

_(et plus généralement les outils de visioconférence basés sur le logiciel Jitsi Meet)_



**_Framatalk est utilisable depuis son navigateur, il n'y a rien à installer._**

La crise sanitaire de 2019-2020 a eu pour effet de propulser l’usage des logiciels de visioconférence à une échelle de masse. Jitsi, qui est un logiciel disponible sous licence libre, figure au titre des solutions souvent présentées pour « maintenir l’activité à distance, gratuitement ».

En réalité quand on évoque Jitsi c’est souvent de **Jitsi Meet** dont il est question. On peut considérer que Jitsi Meet est une déclinaison de Jitsi.

Jitsi Meet est un service en ligne qui, contrairement à Jisti, ne nécessite aucune installation particulière sur l’ordinateur de l’utilisateur final. Le principe de fonctionnement est le suivant : l’utilisateur se connecte sur un serveur Jitsi Meet, **par l’intermédiaire d’un simple navigateur web**, afin de participer à une visioconférence ou d’en créer une.

Le fonctionnement de Jitsi Meet nécessite donc deux parties :

- le « côté client », lequel, comme nous venons de le voir, est assuré par un navigateur web,

- le « côté serveur », qui permet de faire fonctionner le service.

Le présent support ne vise qu’à proposer un accompagnement pour utiliser Jitsi Meet, depuis son navigateur.

Nous n’aborderons nullement le logiciel jitsi, qui doit être installé sur l’ordinateur, pas plus que le logiciel qu’il est nécessaire d’installer, côté serveur, pour utiliser le service Jitsi Meet, depuis son navigateur.

Pour participer à une visioconférence, vous devez avoir :

- un ordinateur ou une tablette ou un smartphone et, le cas échéant, une webcam si votre matériel n’est pas déjà équipé d’une caméra intégrée (ce tuto n’aborde que l’utilisation à partir d’ordinateurs),

- un matériel à jour sur le plan logiciel (le système d’exploitation, le navigateur et tous les composants logiciels utilisant internet),

- une connexion internet,

La plupart des navigateurs, quel que soit le système d’exploitation (Windows, MacOS, GNU-Linux, etc.), peuvent être utilisés pour participer à une visioconférence sur Jitsi Meet.

Néanmoins, certaines fonctionnalités ne sont pleinement accessibles qu’à partir du navigateur Chrome ou de ses dérivés : Google Chrome, Chromium, Microsoft Edge Chromium, etc.

J’aurais donc tendance à conseiller d’installer préalablement une version de ce type de navigateur pour utiliser ce service dans les meilleures conditions.

Dernière recommandation, en particulier, si vous ne vivez pas en solitaire et que l’expérience de visioconférence a lieu chez vous : l’utilisation d’un casque audio s’avère très utile, pour ne pas dire, indispensable.

Cela, certes, n’empêchera pas d’émettre le signal de votre voix &mdash;de toute façon, il faut bien admettre que l’usage massif du téléphone portable nous a définitivement vacciné de toute espérance de révolte contre ces fausses soliloquies compulsives auxquelles nous sommes désormais, en permanence, exposés&mdash; mais, en délimitant la sortie du son à vos oreilles, vous pourrez peut-être espérer limiter les dégâts, en évitant ainsi de déballer l’intégralité de la conversation dans votre salon, pour n’en garder qu’une partie tronquée ; votre voix, votre voix sans répliques, votre voix qui avance masquée, mais votre voix quand même, laquelle, à défaut d’être, pour vos proches, toujours totalement compréhensible (surtout dans ce contexte), n’en demeure pas moins identifiable, et donc, peut-être, un peu moins inquiétante, non ?



## Sommaire

[Service meet.jit.si et instances de Jitsi Meet](#service-meetjitsi-et-instances-de-jitsi-meet)

[Pas d’obligation de créer un compte](#pas-dobligation-de-créer-un-compte)

[À propos de la gratuité](#à-propos-de-la-gratuité)

[Répondre à une invitation](#répondre-à-une-invitation)

[Copier l’URL de la réunion pour l’envoyer à des participants](#copier-lurl-de-la-réunion-pour-lenvoyer-à-des-participants)

[Tester le son (entrée et sortie)](#tester-le-son-entrée-et-sortie)

[Arrivée d’un nouveau participant](#arrivée-dun-nouveau-participant)

[Affichage en mode mosaïque](#affichage-en-mode-mosa%C3%AFque)

[Options de sécurité](#options-de-sécurité)

[Gestion type de la prise de parole](#gestion-type-de-la-prise-de-parole)

[Utiliser le chat lors de la visioconférence](#utiliser-le-chat-lors-de-la-visioconférence)

[Utiliser le chat privé](#utiliser-le-chat-privé)

[Exclure un participant](#exclure-un-participant)

[Régler la qualité vidéo selon la bande passante](#régler-la-qualité-vidéo-selon-la-bande-passante)

[Partager son écran](#partager-son-écran)

[Quitter la visioconférence](#quitter-la-visioconférence)

[Créer une nouvelle visioconférence](#créer-une-nouvelle-visioconférence)

[Auteur du tutoriel, licence et compléments](#tutoriel-réalisé-par-françois-animateur-des-ateliers-multimédia-du-centre-culturel-de-la-ville-des-lilas)





## Service meet.jit.si et instances de Jitsi Meet

Jitsi Meet est un service en ligne « décentralisé ». Cela signifie que, contrairement à la plupart des autres services de visioconférence (par exemple : Zoom, Team, Skype, etc.), les utilisateurs de toutes les visioconférences présentées sous Jitsi Meet ne sont pas obligés de se connecter à un seul et unique serveur.

C’est la raison pour laquelle, j’évoquais dans mon introduction « l’installation du logiciel, côté serveur » car, contrairement aux autres services commerciaux de visioconférence, rien n’empêche à un « informaticien » d’installer le logiciel serveur de Jitsi Meet sur n’importe quelle machine GNU/Linux et de proposer ainsi sa propre instance personnalisée du service de visioconférence.

Il existe donc plusieurs instances du logiciel Jitsi Meet, chacune étant installée sur un serveur différent. Je vous recommande de consulter, à ce sujet, [la page du wiki des Chatons](https://wiki.chatons.org/doku.php/la_visio-conference_avec_jitsi).

Par contre, contrairement à d’autres types de services en ligne décentralisés (par exemple [Mastodon](https://fr.wikipedia.org/wiki/Mastodon_(réseau_social)) ou [PeerTube](https://fr.wikipedia.org/wiki/PeerTube)), il n’est pas possible de fédérer plusieurs instances différentes de serveurs Jitsi Meet. Concrètement, si vous êtes invité à participer à une visioconférence Jitsi Meet, on vous enverra une adresse spécifique et vous n’aurez pas d’autres moyens d’assister à cette réunion que d’insérer cette adresse dans votre navigateur web.

Incontestablement, l’instance la plus connue et, probablement, la plus utilisée de Jitsi Meet est disponible à l’adresse suivante : [https://meet.jit.si](https://meet.jit.si) (c’est également à partir de cette adresse qu’a été réalisée la mise en situation de ce tutoriel). Il ne s’agit, en fait, que du serveur de l’équipe (entreprise ?) qui a développé le programme.

Il faut savoir que Jitsi (donc Jitsi Meet) a été racheté en 2018 par 8x8, une entreprise, elle-même spécialisée dans la visioconférence et la VOIP. Il est donc possible que le service proposé par [https://meet.jit.si/](https://meet.jit.si/) soit conduit à évoluer.

Déjà, aujourd’hui, lorsque l’utilisateur quitte une visioconférence depuis [https://meet.jit.si/](https://meet.jit.si/), on lui propose de basculer sur 8x8 :

![Copie d'écran : fin de visioconférence sur le site de Jitsi Meet](images/01_01Jitsi.png "Copie d'écran : fin de visioconférence sur le site de Jitsi Meet")

![Détail - fin de visioconférence sur le site de Jitsi Meet](images/01_02Jitsi.png "Détail - fin de visioconférence sur le site de Jitsi Meet")



Il n’en reste pas moins que, même si le service [https://meet.jit.si/](https://meet.jit.si/) venait à évoluer au point de disparaître (hypothèse probable), le **logiciel** Jitsi Meet est toujours sous licence libre et rien n’empêche, comme je l’indiquais plus haut, de trouver [une association permettant d’utiliser ce service sur une autre instance](https://wiki.chatons.org/doku.php/la_visio-conference_avec_jitsi#instances_publiques_deployees).

## Pas d’obligation de créer un compte

Autre particularité de Jitsi Meet par rapport à la plupart des autres services de visio : quelle que soit l’instance utilisée (donc, y compris sur [https://meet.jit.si/](https://meet.jit.si/)), vous n’aurez pas à créer de compte !

Que ce soit pour répondre à une invitation ou pour créer de nouvelles séances de visioconférence, on ne vous demandera nullement de laisser des informations personnelles de connexion telles que votre identité, votre mail, pour créer un compte avec un mot de passe, etc.

Si vous souhaitez créer une réunion, vous n’aurez qu’à indiquer un nom (ou n’importe quelle chaîne de caractère) pour constituer une URL personnalisée sur le service (nous verrons cela à la fin du tuto).

Si vous devez rejoindre une réunion il faudra seulement insérer l’URL qui vous a été communiquée dans le navigateur, puis indiquer un nom ou un pseudo afin de vous identifier lors de la visioconférence.

Difficile de faire plus simple !


## À propos de la gratuité

La plupart des instances de Jitsi Meet sont souvent disponibles gratuitement et sans limitation de durée. Super.

Pour autant, faut-il considérer, à l’instar d’une certaine presse informatique mainstream qui semble faire son beurre de cet argumentaire racoleur, que la gratuité représente forcément le critère déterminant dans « le choix d’un service en ligne » ?

Pas sûr.

En particulier, si vous utilisez, tel que je l’évoquais plus haut, [un service associatif décentralisé](https://chatons.org/), vous serez peut-être conduit à adopter un autre positionnement.

La mise en place de serveurs implique des coûts de maintenance technique. Qui paie ? Au nom de quoi faudrait-il, sous prétexte qu’il s’agit de technologies numériques, que tout soit nécessairement gratuit ?

D’ailleurs, quelle est la contre-partie de la soit-disant gratuité des réseaux sociaux ?

Le mode associatif ne repose-t-il pas sur des logiques de participation d’une autre nature que celle qui vous place d’emblée dans la posture de consommation passive ?

Plus généralement, avant d’avoir recours à un service ou un logiciel gratuit, je vous invite à essayer de vous documenter sur votre projet afin de répondre aux questions suivantes : quel est le modèle économique du logiciel ? Quelle est la contrepartie de la gratuité ?


## Répondre à une invitation

Venons-en à la mise en situation.

Je partirai d’abord de ce contexte précis, au moment du deuxième confinement de novembre 2020, lorsque j’ai envoyé des messages électroniques aux usagers des ateliers multimédia pour les inviter à poursuivre l’activité des ateliers multimédia en visioconférence.

Nous verrons, ensuite, à la fin du présent tuto, comment faire pour créer une séance de visioconférence.

Pour l’instant, mettons-nous dans la peau de l’un de ces usagers, que nous appellerons, par commodité, « Totoche » (ce personnage fictif est interprété, ci-dessous, par moi-même).

Nous sommes le mercredi 4 novembre, peu avant 19h00 et Totoche décide d’ouvrir le message électronique que je lui ai envoyé.

Il s’agit donc d’une invitation à se connecter pour participer à la visioconférence de l’atelier photomontage numérique, qui se tient le mercredi à 19 heures.

Pour assister à la conférence, il faudra, semaine après semaine, insérer la même adresse URL dans la barre d’adresse du navigateur (d’où la proposition de créer un marque-page au moment de la première connexion pour éviter d’être obligé de retourner toutes les semaines sur la même invitation) :

![courriel d'invitation](images/01_jitsi.png "courriel d'invitation")


*Comme vous pouvez le constater ci-dessus, pour des raisons de confidentialité, j’ai masqué toutes les adresses (mail et web) sur ce tuto.*

Totoche clique sur le lien du message, ce qui ouvre le navigateur à l’adresse URL :

![Copie d'écran - navigateur](images/02_jitsi.png "Copie d'écran - navigateur")



## Copier l’URL de la réunion pour l’envoyer à des participants

Après un petit moment nécessaire au chargement de l’interface, le navigateur (Chromium) de Totoche affiche ceci :

![formulaire de connexion](images/03_jitsi.png "Formulaire de connexion")


Le formulaire de connexion présente l’adresse URL de la visioconférence et, juste en-dessous, un champ dans lequel les participants indiquent leur identité.

Lors de la première connexion &mdash;ce qui n’est pas indiqué sur ce tuto&mdash; le navigateur demande si vous acceptez que meet.jit.si accède à votre webcam et votre micro.

Sauf si vous ne souhaitez pas que l’on vous voie et que l’on vous entende lors de la conférence, il faut bien évidemment répondre par l’affirmative à cette question. Remarquez que, dès cet écran de connexion, des icônes pour activer ou désactiver votre webcam et votre micro sont disponibles :

![Icônes pour activer ou désactiver webcam et micro](images/04_jitsi.png "Icônes pour activer ou désactiver webcam et micro")


Totoche &mdash;oui, c’est bien lui&mdash; arrive dans la visioconférence.

![Selfie de Totoche](images/05_jitsi.png "Selfie de Totoche")


Il observe cette invitation, placée à l’angle supérieur droit de la fenêtre, qui lui propose d’avaler deux cuillères à soupe de GAFAM.

Totoche décline la proposition en fermant la fenêtre.

Notez qu’il existe une case à cocher qui permet de *Ne plus montrer ceci* : contrairement à Totoche, j’ai cliqué sur cette case avant de fermer la fenêtre, ce qui me permet de ne plus être importuné, par ce message inapproprié.

![Proposition d'installation de l'extension Chrome pour Google Calendar et Office 365](images/06_jitsi.png "Proposition d'installation de l'extension Chrome pour Google Calendar et Office 365")


Totoche est, pour l’instant la seule personne présente dans la visioconférence.

![Pas d'autre personne en ligne](images/07_jitsi.png "Pas d'autre personne en ligne")



## Tester le son (entrée et sortie)

Profitons-en pour faire le tour du proprio. Pour cela Totoche clique sur les trois petits points placés à l’angle inférieur droit de la fenêtre, ce qui permet de développer un menu. Il clique ensuite sur les *Paramètres* :

![Menu des paramètres](images/08_jitsi.png "Menu des paramètres")

Ici, on peut vérifier si le matériel (caméra et micro) est correctement reconnu. Cela peut être nécessaire, par exemple, pour la sortie audio, si vous avez plusieurs cartes son. C’est donc là que vous pouvez tester le micro (une jauge s’anime avec le signal émis), ainsi que la sortie audio (haut-parleur ou casque).

![Tests](images/09_jitsi.png "Tests")


## Copier l’URL de la réunion pour l’envoyer à des participants

Totoche clique ensuite sur cette icône en forme de silhouette :

![Invitation : affichage de l'url](images/10_jitsi.png "Invitation : affichage de l'url")

Cela permet d’ouvrir une petite fenêtre dans laquelle s’affiche l’adresse URL de la réunion (ici encadrée) :

![URL de la réunion](images/11_jitsi.png "URL de la réunion")

On peut cliquer sur ce champ...

![Invitation : copie de l'url](images/12_jitsi.png "Invitation : copie de l'url")


… afin de copier l’adresse (dans cette zone de mémoire temporaire de votre ordinateur que l’on appelle le « presse-papier ») puis, ensuite, coller cette adresse URL dans un mail afin d’inviter des personnes à cette réunion.

C’est, à peu de choses près, ce que j’ai fait pour informer les adhérents que les ateliers se déroulaient désormais en visioconférence.

Remarquez que l’URL que nous avons copiée est exactement la même que celle qui est affichée dans la [barre d’adresse](https://lilapuce.net/Saisie-dans-la-barre-d-adresse) et qu’il serait possible d’arriver au même résultat en cliquant directement sur cette URL, depuis la barre d’adresse, pour la copier puis la coller dans le mail, à l’aide d’un raccourci clavier ([voir support maison](https://lilapuce.net/Recap-Copier-couper-coller)).

![Confirmation lien copié](images/13_jitsi.png "Confirmation lien copié")

Totoche ferme cette petite fenêtre.

![Clic de fermeture](images/14_jitsi.png "Clic de fermeture")


## Arrivée d’un nouveau participant

Une autre personne, dont le nom commence par F, arrive dans la réunion de visioconférence.

![Icône F au centre de l'écran](images/15_jitsi.png "Icône F au centre de l'écran")

Cette autre personne s’appelle François.

![Vue de la deuxième personne connectée](images/16_jitsi.png "Vue de la deuxième personne connectée")



## Affichage en mode mosaïque

François s’affiche à présent en grand dans l’écran de Totoche. Oui, je sais, il y a un petit air de ressemblance entre Totoche et François... Voilà qui jette probablement le trouble !

![Vue plein écran](images/17_jitsi.png "Vue plein écran")

Est-ce à cause de cela que Totoche clique sur cette icône qui permet d’activer et de désactiver la présentation en mode mosaïque ?

![Bouton mosaïque](images/18_jitsi.png "Bouton mosaïque")

Voici à quoi correspond, sur l’ordinateur de Totoche, le mode affichage mosaïque :

![Vue mosaïque](images/19_jitsi.png "Vue mosaïque")

Bon, allez, je vais vous fournir un petit secret de fabrication sur ce tuto : j’ai décidé d’interpréter non seulement Totoche mais aussi François, incroyable non ?

Contrairement à ce que vous pourriez penser, mon intention n’était pas de montrer qu’il existe probablement des personnes qui n’ont pas d’autre moyen de faire le point sur la situation planétaire qu’en se connectant à elles-mêmes.

Pas du tout : cette partie de solo intégrale s’explique uniquement par le fait que je n’ai pas l’habitude d’impliquer qui que ce soit dans mes tutoriels et que, s’il est toujours souhaitable de pouvoir changer ses habitudes, il me semble difficile de revenir en arrière sur ce point, aujourd’hui.

Donc, afin de clarifier la suite de ma présentation, voici quelques repères qu’il me semble important de vous proposer à ce moment précis du tuto :

- J’ai pris deux ordinateurs différents pour réaliser cette mise en situation.

- L’ordinateur portable de Totoche, nous donne cette vue, légèrement en contre-plongée, reconnaissable à cette tonalité verte. Totoche utilise le système d’exploitation [Ubuntu Studio](https://fr.wikipedia.org/wiki/Ubuntu_Studio). Ci-dessus, la vue de Totoche est à droite.

- L’ordinateur de François est une station Apple, placée en hauteur, ce qui donne, au contraire de celui de Totoche, ce point de vue plongeant, avec une couleur plus neutre. François utilise le système d’exploitation [Ubuntu](https://fr.wikipedia.org/wiki/Ubuntu_(syst%C3%A8me_d%27exploitation)) (avec sa barre de lanceurs caractéristique, placée sur le côté gauche du bureau, que vous verrez parfois quand je montrerai, sur ce tuto, ce qui se passe sur l’écran de François). Ci-dessus, la vue de François est à gauche.

- Attention, au fil de la conversation, les petites fenêtres qui affichent l’image des participants à la réunion sont susceptibles de changer de place, notamment en raison d’un départ ou d’une arrivée. Donc il ne sert à rien de se repérer à partir de la position de ces fenêtres pour identifier les différentes personnes.

- Attention (bis) : si vous souhaitez renouveler le type d’expérience que j’ai mis en œuvre pour réaliser ce tuto, à savoir ouvrir, dans la même pièce deux machines connectées en même temps à la même visioconférence, je vous conseille de désactiver le son sur l’une des machines, faute de quoi vous risqueriez d’obtenir un effet de larsen très très désagréable.

- Le point de vue principal de ce tuto sera celui de Totoche.

- Considérons que la réunion n’est pas encore commencée. D’autres participants doivent arriver mais Totoche et François sont en avance. Ils s’échangent quelques amabilités.


## Options de sécurité

Totoche continue d’explorer l’interface de la visioconférence en cliquant sur cette icône :

![Icône options de sécurité](images/20_jitsi.png "Icône options de sécurité")

On constate ici qu’il est possible de créer un statut de modérateur donnant lieu, comme cela existe sur d’autres logiciels de visioconférence, à la constitution d’une salle d’attente, dans laquelle seront placés et filtrés les participants avant d’entrer dans la réunion.

De même, le modérateur pourra créer un mot de passe pour accéder à la réunion. Enfin, il est expliqué (en anglais) que la séance peut faire l’objet d’un chiffrement mais que cette fonctionnalité est expérimentale, donc peu fiable.

Ces options sont, ici, désactivées par défaut :

![Écran options de sécurité](images/21_jitsi.png "Écran options de sécurité")



## Gestion type de la prise de parole

L’usage veut, lorsque l’on rentre en séance de visioconférence, que tous les participants coupent leur micro afin d’éviter un effet de brouhaha.

La logique étant que l’animateur, le conférencier ou le responsable démarre la séance dans une totale sérénité et distribue ensuite les prises de parole, après que les demandes aient été formulées, comme nous allons le voir, en bonne et due forme.

Déjà, il faut rappeler qu’il s’agit d’un usage et que, comme tout usage, celui-ci demande à être réévalué en fonction de situations précises. Ainsi, pour ce qui concerne nos ateliers, je demande au contraire aux participants de ne pas couper leur micro lors des premières connexions car j’ai constaté que beaucoup de personnes se trouvent en difficulté lorsqu’elles ont le micro coupé car, dans ces conditions, elles ne peuvent pas vérifier si les autres personnes les entendent correctement.

Dans notre cas, les micros de Totoche et François sont activés ; ce que nous avons constaté au moment de la connexion à la réunion.

Voyons à présent comment il est possible de couper le micro d’un participant.

Sur toutes les fenêtres des participants &mdash;à l’exception de la sienne&mdash; on trouve cet alignement de trois points placés à l’angle supérieur droit :

![Trois points](images/22_01jitsi.png "Trois points")


En cliquant à cet emplacement, sur la fenêtre de François, Totoche fait apparaître ce menu :

![Menu des trois points](images/22_jitsi.png "Menu des trois points")


Il clique ensuite sur la première option, permettant de couper le micro, d’où cette boîte de dialogue :

![Couper le micro](images/23_jitsi.png "Couper le micro")


Imaginons que Totoche valide la proposition et décide donc de couper le micro de François.

Sur l’écran de Totoche, l’image de François affichera alors cette icône du micro barré, placée à l’angle inférieur gauche :

![Icône micro barré](images/24_jitsi.png "Icône micro barré")


Voyons, maintenant ce qui se passe sur l’écran de François :

![Info sur écran de François](images/25_jitsi.png "Info sur écran de François")


Imaginons que François, ayant compris que c’est Totoche qui mène désormais la danse, laisse ce dernier gérer les prises de parole.

Un peu plus tard, François souhaite justement s’exprimer. Pour cela il lui faudra cliquer sur cette icône :

![Icône lever la main](images/26_jitsi.png "Icône lever la main")


Sur l’écran de Totoche, à l’emplacement de la fenêtre de François, s’affiche alors cette petite icône bleue qui prévient que François veut prendre la parole :

![Icône main levée](images/27_jitsi.png "Icône main levée")


Totoche donne son autorisation, par exemple en disant : « Ha ! je vois que François souhaite s’exprimer... c’est bon François, tu peux y aller. »

Du coup François clique sur cette icône de micro qui était barrée...

![Icône activer le micro](images/28_jitsi.png "Icône activer le micro")


Pour libérer le micro et parler :

![Icône micro ouvert](images/29_jitsi.png "Icône micro ouvert")


Une fois qu’il aura terminé son intervention, il n’oubliera pas immédiatement de cliquer à nouveau sur l’icône de la main pour la désactiver.

S’il ne clique pas une deuxième fois pour désactiver la main, François sera toujours identifié dans l’assistance comme souhaitant s’exprimer (ce qui ne manquerait pas de susciter quelques désagréables moqueries, n’en doutons pas !)

Enfin, après avoir parlé, il lui faudra à nouveau cliquer sur l’icône de son micro pour couper à nouveau l’entrée son et ainsi laisser Totoche interpréter en toute quiétude son rôle de maître de cérémonie.

![Icône lever la main](images/30_jitsi.png "Icône lever la main")



## Utiliser le chat lors de la visioconférence

Tous les logiciels de visioconférence donnent la possibilité de lancer des conversations de messagerie instantanée, autrement nommée *chat*. Pour la plupart de ces logiciels, d’ailleurs, la fonctionnalité de messagerie préexistait à celle de visioconférence.

Il est indispensable que tous les participants aux réunions de visioconférence sachent que cette fonctionnalité de messagerie au clavier existe.

Pour différentes raisons (problème de traitement du son, webcam inexistante, bande passante limitée, etc.) certaines personnes qui ne peuvent techniquement pas avoir accès aux fonctions audio et vidéo en entrée auront quand même la possibilité de participer à la réunion en s’exprimant par la messagerie.

J’ai même assisté à des conférences où la seule possibilité donnée à l’audience se résumait à utiliser le chat.

Retour sur l’écran de Totoche. Il souhaite engager une conversation avec les autres participants (je sais : pour l’instant il n’y a que François, mais essayez d’avoir un peu d’imagination, enfin). Il clique sur cette première icône, en bas à gauche :

![Icône chatter](images/31_jitsi.png "Icône chatter")

Cela ouvre un espace, sur toute la hauteur du côté gauche de la fenêtre. La ligne de saisie se trouve tout en bas, il faut cliquer pour placer le point d’insertion et taper son texte. Il est possible d’insérer des émojis, de la même façon que dans la messagerie électronique de votre ordinateur ou les SMS de votre smartphone. Dans la ligne de saisie l’émoji s’affichera sous la forme d’un code. Par exemple, ci-dessous l’émoji se présente sous la forme d’un :+1:

Totoche valide la saisie de son message en appuyant sur la touche Entrée du clavier.

![Chat](images/32_jitsi.png "Chat")


Ce message s’affiche en bleu sur son écran. Le code :+1: correspond bien à l’émoji du « j’aime » (pouce levé).

![Chat pouce levé](images/33_jitsi.png "Chat pouce levé")


Toujours sur l’écran de Totoche, s’affiche alors une réponse de François en gris.

Les couleurs sont inversées sur l’écran de François.

![Chat](images/34_jitsi.png "Chat")



## Utiliser le chat privé

Il existe aussi la possibilité d’engager une conversation privée, avec une seule personne.

Imaginons que d’autres participants rejoignent la visioconférence.

Si Totoche veut envoyer un message qui ne sera visible que pour François, il lui faudra à nouveau cliquer sur la suite de points, placée en haut, à droite, sur l’image de François et cliquer sur l’option suivante :

![Envoyer un message privé](images/35_01_jitsi.png "Envoyer un message privé")


Totoche n’aura plus alors qu’à taper son message dans le même champ de saisie du Chat que nous avons déjà vu et ce message ne s’affichera, bien entendu, que sur l’écran de François.

Pour les distinguer des Chats envoyés à tous les participants, les messages privés s’affichent dans un bandeau rouge, sur lequel est précisé le nom du destinataire.


## Exclure un participant

Puisque nous sommes sur ce petit menu qui s’affiche sur chacun des participants, après avoir cliqué sur l’angle supérieur droit, profitons-en pour présenter une fonction importante à connaître : exclure.

Contrairement à ce que l’on pourrait penser, cette fonctionnalité ne consiste pas seulement à dégager de la réunion une personne indésirable.

Imaginons, que l’affichage de la réunion fonctionne mal sur le navigateur de François. La visio plante uniquement pour lui ; son image se fige sur l’écran de Totoche et François est obligé de recharger l’onglet de son navigateur, pour retourner dans la réunion, tel que Jitsi Meet le lui propose en envoyant un message d’erreur.

Cette situation, qui n’a absolument rien d’inhabituel, peut avoir comme effet de charger deux fois l’image de François sur l’ordinateur de Totoche (et des autres participants).

Panique à bord : à nouveau on risque alors d’avoir un effet sonore de feed-back avec un larsen insupportable.

Dans ce cas il n’y aura pas d’autre solution que d’exclure la première image de François, celle qui a planté, mais qui est restée dans l’interface de la visioconférence et, dans le cas présent, c’est Totoche qui devra procéder à cette exclusion !

![Exclure](images/35_02_jitsi.png "Exclure")




## Régler la qualité vidéo selon la bande passante

Après avoir discuté par la messagerie instantanée avec François de la qualité du signal émi et reçu pendant la visioconférence, Totoche décide de revenir sur les options puis, sur le menu, il clique cette deuxième proposition :

![Ajuster la qualité vidéo](images/35_jitsi.png "Ajuster la qualité vidéo")


La boîte de dialogue donne la possibilité de déplacer le curseur pour adapter la qualité de la vidéo en fonction de la bande passante.

![Ajuster la qualité vidéo : curseur](images/36_jitsi.png "Ajuster la qualité vidéo : curseur")


Cela peut s’avérer utile si, par exemple, l’image vidéo se fige sur l’écran de Totoche. En baissant la qualité on essaiera de gagner en fluidité. Attention toutefois : en glissant sur « Bande passante faible » Totoche couperait complètement le signal vidéo (ce qui représente toutefois une option valide si l’on est en bas débit).

![Ajuster la qualité vidéo : curseur](images/37_jitsi.png "Ajuster la qualité vidéo : curseur")



## Partager son écran

Venons-en, de mon point de vue, à la fonctionnalité la plus intéressante de la visioconférence : le partage d’écran.

Le principe est très simple : Totoche souhaite montrer à François des photos qui sont enregistrées sur son ordinateur, puis ensuite, il veut lui demander des explications sur une procédure technique détaillée qui nécessite que soient enchaînées l’ouverture de plusieurs fenêtres et boîtes de dialogue dans un programme graphique (Gimp).

Grâce à la fonctionnalité de partage d’écran Totoche pourra enfin parler de choses sérieu... heu pardon, il pourra montrer exactement ce qu’il veut montrer et, cela, dans les meilleures conditions, puisque François verra alors dans sa fenêtre de visioconférence une reproduction fidèle de ce qui se passe sur l’écran de Totoche.

Pour activer le partage d’écran, Totoche clique sur cette icône, celle qui est placée à gauche de la main :

![Icône partage d'écran](images/38_jitsi.png "Icône partage d'écran")


C’est notamment lors de cette étape précise que l’on apprécie d’avoir un navigateur de type Chromium, car si l’on est sur Firefox, par exemple, l’interface est beaucoup moins conviviale que celle présentée ci-dessous, par cette fenêtre :

![Menu partage d'écran](images/39_jitsi.png "Menu partage d'écran")

Le logiciel propose trois options de partage. Commençons par la deuxième qui indique « Partager la fenêtre de l’application ».


![Partager la fenêtre de l’application](images/40_jitsi.png "Partager la fenêtre de l’application")

Dans le cas présent, on voit que Totoche a ouvert quatre fenêtres (dont celle qui affiche la visioconférence). S’il choisit cette option il devra ensuite cliquer sur l’une des quatre fenêtres qu’il souhaite partager.

Petite précision quand même : je déconseille fortement de partager la fenêtre de visioconférence car, par définition, la visio étant déjà partagée, vous risqueriez alors d’obtenir une imbrication de votre conversation dans une présentation de mise en abyme du plus bel effet psychédélique mais assez peu explicite d’un point de vue cognitif, voire même, carrément flippante. Dans le genre *bad trip* ou *good trip*, c’est selon, tu vois.

Une fois cette réserve proposée, il me faut expliquer pourquoi je n’utilise quasiment jamais cette option « Partager la fenêtre de l’application » : admettons que Totoche choisisse, comme je l’ai indiqué plus haut, de partager ainsi sa fenêtre ouverte sous Gimp, pour montrer à François un enchaînement de procédures techniques.

Ceux et celles qui connaissent ce programme savent que l’interface de Gimp se présente sous la forme d’une multitude de fenêtres et autres boîtes de dialogue qui viennent s’afficher devant la fenêtre principale du logiciel.

Or, en choisissant « Partager la fenêtre de l’application » François ne verra que la fenêtre principale de Gimp et aucune autre. Autrement dit le partage, dans ces conditions, ne permettra pas de présenter les autres fenêtres et boîtes de dialogues de Gimp. Donc cette option n’est pas adaptée à ce que nous voulons faire.

![Partager la fenêtre de l’application](images/40_jitsi.png "Partager la fenêtre de l’application")

La troisième option indique « Onglet Chromium », autrement dit, on peut de cette façon partager uniquement l’un des onglets ouverts dans la fenêtre active du navigateur.

Cette solution est intéressante (en rappelant, toutefois, qu’il vaut mieux éviter de partager l’onglet de la visioconférence).

Le partage d’un onglet du navigateur peut effectivement représenter la meilleure solution si, par exemple, Totoche ne souhaite monter à François que certains onglets du navigateur et rien d’autre de son ordinateur, mais nous avons vu que cette hypothèse ne correspond pas à notre cas de figure.

![Partager un onglet](images/41_jitsi.png "Partager un onglet")

Il faut donc venir sur la première proposition : « L’intégralité de votre écran » laquelle correspond exactement à la situation évoquée.

![Partager tout l'écran](images/42_jitsi.png "Partager tout l'écran")


Après avoir cliqué une première fois sur l’option de partage de l’écran (ci-dessus), Totoche doit cliquer une deuxième fois sur la représentation de son écran, affichée juste en dessous, pour qu’apparaisse un encadré bleu :

![Encadré](images/43_jitsi.png "Encadré")


Après quoi, il lui sera possible de cliquer sur le bouton « Partager » (ci-dessus).

Totoche voit alors quelque chose qui ressemble à ceci :

![Écran partagé](images/44_jitsi.png "Écran partagé")


Ce bandeau affiché en bas de la fenêtre permet de savoir que le partage de son écran est effectif.

![Bandeau Écran partagé](images/45_jitsi.png "Bandeau Écran partagé")


Pour les raisons que j’ai évoquées plus haut (risque d’affichage de mise en abyme), Totoche doit se hâter de quitter la fenêtre active, à savoir la visioconférence… mais attention, **il ne faut pas fermer la fenêtre mais la réduire !**

En fermant le navigateur, Totoche quitterait la visioconférence or il veut rester dans la visio mais, justement, il veut y rester pour partager d’autres fenêtres que celle de la visio !

Voilà pourquoi il faut réduire et non fermer la fenêtre du navigateur.

L’interface graphique de bureau (Xfce) du système d’exploitation de Totoche est assez proche, de ce point de vue, de celle de Windows : pour réduire il faut cliquer sur cette barre, placée à l’angle supérieur de la fenêtre du navigateur.

Le principe est exactement le même sur tous les systèmes d’exploitation mais l’interface graphique n’est pas toujours présentée de la même façon. Par exemple sur Mac OS, les contrôles des fenêtres sont des petites puces de couleur placées à gauche.

Pour ne pas être mis en difficulté, vous devez savoir, sur votre ordinateur :

- comment réduire une fenêtre

- comment récupérer cette fenêtre réduite pour l’ouvrir à nouveau : sur Windows c’est la barre de tâches, sur Mac c’est le Dock, sur Ubuntu c’est la barre de lanceurs, etc.

![Réduire une fenêtre](images/46_jitsi.png "Réduire une fenêtre")


Une fois qu’il a réduit la fenêtre de son navigateur, Totoche affiche une fenêtre de Gimp :

![Fenêtre](images/47_jitsi.png "Fenêtre")


Voici comment s’affiche le partage d’écran, sur l’écran de François :

![Écran partagé](images/48_jitsi.png "Écran partagé")


Totoche décide ensuite de n’afficher que son bureau.

![Bureau de Totoche](images/49_jitsi.png "Bureau de Totoche")


Puis il ouvre son explorateur de fichiers.

![Menu de Totoche](images/50_jitsi.png "Menu de Totoche")


Tout ce qui s’affiche sur son écran s’affiche aussi sur l’écran de François.

![Fichiers de Totoche](images/51_jitsi.png "Fichiers de Totoche")


Y compris cette fenêtre de Libre Office Writer, avec une boîte de dialogue, placée devant la fenêtre principale du traitement de texte :

![LIbre Office Writer de Totoche](images/52_jitsi.png "Libre Office Writer de Totoche")


Voici l’écran de Totoche, tel qu’il s’affiche alors sur l’écran de François :

![Écran partagé](images/53_jitsi.png "Écran partagé")


Pour arrêter le partage, Totoche devra juste cliquer sur ce bouton :

![Arrêter le partage](images/54_jitsi.png "Arrêter le partage")


Puis il lui faudra retourner dans la visioconférence en affichant à nouveau son navigateur web !


## Quitter la visioconférence

Quand le moment est venu de quitter la visioconférence, après les salutations d’usage, il est recommandé que chacune des personnes clique sur cette icône rouge :

![Quitter la visioconférence](images/55_jitsi.png "Quitter la visioconférence")



## Créer une nouvelle visioconférence

Nous avons vu comment répondre à une invitation pour participer à une visioconférence sur Jitsi Meet. Pour terminer, je vous propose de voir comment créer votre propre réunion sur le service meet.jit.si.

Affichez cette URL dans votre navigateur :

![meet.jit.si](images/56_jitsi.png "meet.jit.si")


Cliquez ensuite dans le champ puis tapez le nom de votre groupe. Essayez d’indiquer un nom assez long pour vous assurer que personne d’autre que vos invités ne puissent s’y rendre.

![Choix du nom](images/57_jitsi.png "Choix du nom")

Cliquez sur créer.

![Nouvelle conférence créée](images/58_suite.png "Nouvelle conférence créée")


Le tour est joué. Il vous faudra ensuite [récupérer l’adresse URL de la réunion pour l’envoyer à vos correspondants](#copier-lurl-de-la-réunion-pour-lenvoyer-à-des-participants).


### Tutoriel réalisé par François, animateur des ateliers multimédia du centre culturel de la ville des Lilas.

### Site : [Lilapuce](https://lilapuce.net/)

Licence [CC-BY-SA](https://creativecommons.org/licenses/by-sa/2.0/)

[Un autre tutoriel dans ce pdf](https://kb.easydns.com/wp-content/uploads/2020/05/Jitsi-User-Guide-French.pdf)
