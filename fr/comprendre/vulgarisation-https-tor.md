# Vulgarisation sur https et TOR

## Une petite histoire

Imaginer une zone pavillonnaire avec différentes maisons dont celle de votre ami.

**Cas 1** : Sa maison a des murs transparents. On vous voit aller chez lui, on peut entendre ce que vous dites et voir ce que vous faîtes.

**Cas 2** : Maintenant, sa maison a des murs pleins. On vous voit aller chez lui, mais on peut plus entendre ce que vous dites et voir ce que vous faites (on met de côté l’aspect micro/caméra). Mais on sait à quelle heure vous êtes venu le voir et quand vous repartez.

**Cas 3** : Si en arrivant de l'extérieur de la zone pavillonnaire, vous entrez dans une première maison, et en ressortez avec un déguisement, puis vous entrez dans une seconde maison au hasard et en ressortez avec un autre déguisement, et entrez enfin dans une troisième maison au hasard et en ressortez avec un autre déguisement, pour enfin entrer chez votre ami. On sait que quelqu’un est entré chez lui, de quelle heure à quelle heure, mais on ne sait pas que c’est vous. A moins de surveiller toutes les maisons, de surveiller toutes les personnes qui entrent et sortent de chaque maison (sachant que chacun en ressort déguisé), ce qui est très compliqué...

**Cas 4** : Si en arrivant de l'extérieur de la zone pavillonnaire, vous entrez dans une première maison, et en ressortez avec un déguisement, puis vous entrez dans une seconde maison au hasard et en ressortez avec un autre déguisement, et entrez enfin dans une troisième maison au hasard et en ressortez avec un autre déguisement, pour enfin entrer chez votre ami. Mais cette fois, vous passez par la porte arrière de la maison, côté jardin. On ne sait même pas qu’une personne est venue dans la maison de votre ami.

## Transposons ça à Internet

**Cas 1** : Dans le premier cas, il s’agit d’une connexion Http classique. Vous utilisez votre navigateur, allez sur un site. Un observateur extérieur peut savoir que vous allez sur le site et quelles pages vous consultez.

**Cas 2** : Dans le deuxième cas, il s’agit d’une connexion Https. Vous utilisez votre navigateur, allez sur un site en mode sécurisé. Un observateur extérieur peut savoir que vous allez sur le site mais ne voit pas quelles pages vous consultez (seul le site le sait et enregistre les traces dans le journal de logs, tout comme votre ami chez qui vous êtes saura ce dont vous lui avez parlé).

**Cas 3** : Dans le troisième cas, il s’agit d’une connexion qui se fait via le réseau Tor. La connexion passe par trois proxys intermédiaires avant d’arriver sur le site web. Je ne détaillerai pas plus le fonctionnement de Tor.

**Cas 4** : Dans le quatrième et dernier cas, il s’agit d’une connexion à un service caché dans Tor, les fameux Hidden services, le fameux *Darknet*. On ne ressort pas du réseau Tor, on entre par une porte particulière sur le site web. Tout comme on entre par une porte dissimulée de la vue de tous chez son ami.
