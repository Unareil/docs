# Synchroniser ses données

Pour synchroniser vos données sur plusieurs appareils, il faut télécharger le client Nextcloud de synchronisation correspondant à votre environnement.

## Télécharger

Le logiciel de synchronisation est disponible pour Windows, Mac OS et GNU/Linux&nbsp;: https://nextcloud.com/install/#install-clients (cliquez sur le bouton de votre système).

## Configurer

Une fois le client installé et lancé, une fenêtre s’ouvre pour vous aider à configurer le client.

Saisissez l'adresse sur serveur : `https://framadrive.org`

![image de l'écran de connexion](images/sync/dsync-server.png)

Puis, votre nom d'utilisateur et votre mot de passe.

![image de la page de saisie des identifiant/mdp](images/sync/dsync-login.png)

Choisissez les dossiers que vous voulez voir synchronisés.

![image de fin de configuration](images/sync/dsync-folders.png)

Enfin, une icône en forme de nuage dans la zone de notification vous
indique que la synchronisation a commencé. En cliquant dessus vous
pouvez voir la listes des fichiers transférés

![image des dossiers synchronisés](images/sync/dsync-end2.png)

![image de l'activité](images/sync/dsync-end.png)
