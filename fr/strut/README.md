# Framaslides

<div class="alert-warning alert">
<b>Déframasoftisons Internet</b> : l’association Framasoft a programmé la fermeture de certains services, pour éviter d’épuiser nos maigres ressources et pour favoriser l’utilisation de petits hébergeurs de proximité (<a href="https://framablog.org/2020/03/03/10-bonnes-raisons-de-fermer-certains-services-framasoft-la-5e-est-un-peu-bizarre/">toutes les infos ici</a>).
<b class="violet">Frama</b><b class="vert">slides</b> fermera ses portes le <b>mardi 11 janvier 2022</b>.
Nous avons réuni des outils pour vous aider à récupérer vos données et à trouver un service similaire <a href="https://alt.framasoft.org/fr/framaslides">sur cette page</a>.
</div>

[Framaslides](https://framaslides.org) est un service en ligne libre qui permet de créer, éditer, voir et partager ses présentations.

* Vous pouvez créer et éditer vos présentations, en choisissant le contenu et en déplaçant les slides par rapport aux autres pour créer des transitions 3D&nbsp;;
* Lancer le diaporama et le publier via une URL&nbsp;;
* Partager des présentations avec un groupe et vous inspirer des modèles publics.

Découvrez ses fonctionnalités et possibilités avec notre [exemple d’utilisation](exemple-d-utilisation.md).

Tutoriel pour bien débuter&nbsp;: [Comment créer sa première présentation](create_first_presentation.html)

Documentation générale&nbsp;:

* Édition des diapositives
  * [Images](pictures.html)
  * [Vidéos](videos.html)
  * [Arrière-plan](background.html)
  * [Raccourcis](shortcuts.html)

* Gestion des présentations
  * [Groupes](groups.md)

Le service repose notamment sur le logiciel libre [Strut](https://strut.io), les sources de Framaslides sont disponibles sur [Framagit](https://framagit.org/framasoft/framaslides/).

### Pour aller plus loin&nbsp;:
-   [Déframasoftiser Internet](deframasoftiser.html)
-   Essayer [Framaslides](https://framaslides.org)
-   Découvrir [Strut, la brique logicielle originelle](https://strut.io/)
-   Participer au [développement de Framaslides](https://framagit.org/framasoft/framaslides/)
-   Un service proposé dans le projet [Dégooglisons Internet](https://degooglisons-internet.org)
-   Son développement a été financé [grâce à vos dons](https://soutenir.framasoft.org).
