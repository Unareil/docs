# Créez ou modifiez une notice

C'est un des points important de l'annuaire [Framalibre](https://framalibre.org) : vous n'y trouverez que ce que d'autres y ont mis. C'est donc à vous d'y placer des ressources libres que vous aimez et voulez partager, ou bien apporter des modifications aux notices existantes.

## Créer une notice

Nous avons fait en sorte que ce soit le plus simple possible :
    
  1. Faites une recherche pour bien vérifier que la notice que vous voulez ajouter n'existe pas déjà ;)
  2. Créez vous un compte (cf. tuto précédent)
  3. Connectez-vous à votre compte
  4. Cliquez  sur "ajouter une notice" toujours présent dans une colonne latérale.


### 6 types de notices différentes

Le bouton "ajouter une notice" vous propose un choix déroulant qui vous mènera vers 6 formulaires différents. Il convient de choisir celui le mieux adapté au type de ressource que vous voulez présenter : 
    
  * **Logiciel** : ajouter un logiciel libre
  * **matériel** : ajouter une référence pour du matériel, outils, dispositifs, etc.
  * **Un article** : ajouter un article issu d'une revue, d'un journal, d'un blog, d'un site... (utile pour la catégorie "S'informer", par exemple)
  * **Livre** : pour signaler une monographie ou un ouvrage collectif
  * **Média** : ajouter une ressource multimédia (video, musique, diaporama...)
  * **Ressource générique** : Ajouter une autre ressource (contenu non spécifique)

> Notez que les types de ressources ne sont pas des catégories : vous pouvez très bien créer une ressource de type **Média**  et la ranger dans la catégorie « électronique » car il s'agit par exemple d'un tutoriel sur un montage électronique. À vous de voir ce qui est le mieux approprié.

### À remplir obligatoirement

Il y a quelque champs à remplir obligatoirement, car sans eux votre notice ne serait pas vraiment utile... 

  * La catégorie (et sous-catégorie) : à choisir parmi celles existantes.
  * Le titre de la ressource (le nom, l'intitulé, le titre, etc.)
  * Le lien officiel : le but de cet annuaire est de fournir les liens des sites officiels des ressources.
  * Les tags (mots clés) : choisir de préférence parmi les tags existants (en tapant les premières lettres), ils permettent de relier la ressource à celles qui lui correspondent. C'est une fonctionnalité importante de l'annuaire car grâce à elle des découvertes sont possibles.
  * La description.


### Les champs complémentaires :

Bien entendu, si les autres champs sont présents, c'est que nous les jugeons importants. Fournir un ou plusieurs visuels de votre ressource (capture d'écran, logo, photo, pochette, etc.) ou un résumé court est essentiel si vous voulez que sa notice donne envie d'en savoir plus.

N'hésitez donc pas à rechercher plus en profondeur (le site officiel ou la page wikipédia de la ressource, par exemple) afin de bien remplir l'ensemble des champs demandés... une fois le ou les visuels fournis, remplir une fiche peut prendre 5 minutes montre en mains...

## Des questions ? 

### La foire aux questions

Nous maintenons à jour une foire aux questions sur Framalibre afin de vous permettre de maîtriser le site en toute autonomie... Vous y trouverez des astuces pour créer votre notice, comme : 

  * [Qu'est-ce qu'une bonne notice ?](https://framalibre.org/faq-page#n50)
  * [Que ne dois-je pas faire ?](https://framalibre.org/faq-page#n216)
  * [Quels tags ?](https://framalibre.org/faq-page#n53)
  * [Comment choisir de bons mots-clés (tags)](https://framalibre.org/faq-page#n317)
  * [Qu'est-ce que le champ « référence interne » ?](https://framalibre.org/faq-page#n52)
  * [À quoi sert le champ « alternative pour » ?](https://framalibre.org/faq-page#n61)
  * [Comment utiliser les champs ?](https://framalibre.org/faq-page#n51)
  * [Quelles licences sont acceptées, lesquelles sont refusées ?](https://framalibre.org/faq-page#n54)

### L'équipe de modération

L'équipe de modération recevra automatiquement les courriels envoyés grâce au formulaire accessible depuis le bouton « [signaler un contenu](https://framalibre.org/node/308) ». 

Bien sûr les possibilités de signalement sont limitées : c'est parce qu'elles ne concernent que les situations où le simple utilisateur n'est pas en mesure d'agir (doublons, malveillances, etc.).

S'il s'agit de modifier une notice, par exemple, vous pouvez le faire directement et même contacter le créateur de la notice.

Si vous désirez faire part d'une amélioration technique de l'annuaire, vous pouvez contribuer en écrivant une issue [sur le dépôt GIT du projet](https://framagit.org/framasoft/framalibre/issues).

Pour rejoindre l'équipe de modération, il faut les retrouver [sur le forum des bénévoles de Framasoft](https://framacolibri.org/t/framalibre-constitution-dune-modo-team/1278).

## Modifier une notice (et le suivi de versions)

Si vous voyez une erreur, un manque, ou une information obsolète dans une notice, nous vous encourageons fortement à proposer une modification, c'est cela qui fait vivre l'annuaire et permet qu'il soit à jour !

Là aussi, vous devrez être connecté·e à votre compte Framalibre afin de pouvoir ptroposer des modifications.

![capture page d'une notice, compte connecté](images/framalibre06.png "Soumettre une mise à jour")

### Comment modifier une notice ?

  1. Allez sur la fiche de la notice
  2. cliquez sur l'oglet modifier au dessus de la notice (à côté de l'onglet "Voir") OU sur "Soumettre une mise à jour"
  3. Effectuez les modifications dans le formulaire.
  4. (optionnel) Relisez à l'aide de l'Aperçu, ou voyez les modifications avec le bouton "Voir les modifications"
  5. Enregistrez votre modification.

À chaque fois qu'une notice est modifiée, son créateur est prévenu par courriel. Il se génère aussi une archive de la notice dans sa version prééxistante. Les différentes versions (l'historique des modifications et les noms des contributeurs) peuvent alors être visualisées et comparées (à la manière d'un wiki).

Ainsi à chaque fois que vous modifiez une notice, vous pouvez commenter votre révision, cela aidera le créateur à comprendre votre intention. Les révisions des notices (lorsqu'il y en a) sont accessible en ajoutant le mot « revisions » à l'adresse de la notice, ainsi : `framalibre.org/content/nom-de-la-notice/revisions` 

### Une fois ma modification proposée...

Ele est automatiquement validée et le créateur originel est prévenu par courriel. 

Dès lors, la personne ayant créé la notice pourra voir vos apports et les modifier le cas échéant.

En cas de litige ou d'absence, notre équipe de modération fera de son mieux pour être présente.

