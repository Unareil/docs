# Framasite

<div class="alert-warning alert">
<b>Déframasoftisons Internet</b> : l’association Framasoft a programmé la fermeture de certains services, pour éviter d’épuiser nos maigres ressources et pour favoriser l’utilisation de petits hébergeurs de proximité (<a href="https://framablog.org/2020/03/03/10-bonnes-raisons-de-fermer-certains-services-framasoft-la-5e-est-un-peu-bizarre/">toutes les infos ici</a>).
Framasite fermera ses portes le <b>mardi 6 juillet 2021</b>.
Nous avons réuni des outils pour vous aider à récupérer vos données et à trouver un service similaire <a href="https://alt.framasoft.org/fr/framawiki">sur cette page</a>.
</div>

[Framasite Wiki](https://frama.site/) est un service libre de création de wikis.

Les Framawiki sont des sites collaboratifs et multi-pages, demandant un poil de connaissances mais restant assez aisés d'usage.

Le service repose sur le logiciel libre [DokuWiki](https://www.dokuwiki.org/start?id=fr:dokuwiki).

---

## Pour aller plus loin&nbsp;:

- Utiliser [Framasite](https://frama.site/)
- [Site de démonstration](https://valvin.frama.wiki/quickstart)
- [Déframasoftiser Framasite wiki](deframasoftiser.md)
- [Astuces](astuces.md)
- [Le site officiel de DokuWiki](https://www.dokuwiki.org/start?id=fr:dokuwiki) (pensez à [les soutenir](https://www.dokuwiki.org/fr:donate) !)
- Un service proposé dans le cadre de la campagne [contributopia](https://contributopia.org/)
