# F-Droid
[<i class="fa fa-arrow-left" aria-hidden="true"></i> Retour à l'accueil](../README.md)

F-Droid est un magasin d'applications libres pour les systèmes Android, en alternative à Google Play.

## Installation de F-Droid

Pour permettre l'installation de F-Droid, vous devez changer un paramètre de votre téléphone. Pour ce faire&nbsp;:

  * ouvrez l'application **Paramètres**
  * sélectionnez **Sécurité**
  * activez l'option **Sources inconnues** le temps de l'installation.

Téléchargez ensuite le fichier d'installation apk de F-Droid en cliquant sur le bouton **Télécharger F-droid** sur la page https://f-droid.org/. Une fois téléchargé ouvrez-le. Procédez à l'installation en cliquant sur **Installer**.

Une fois l'installation effectuée, retournez dans l'application **Paramètres**, puis **Sécurité** et désactivez l'installation d'applications à partir de sources inconnues.

## Utilisation de F-Droid

Les applications dans F-Droid sont classées par catégories, et vous pouvez également faire une recherche dans le titre ou la description des applications. Pour ce faire vous devez&nbsp;:

  1. cliquer sur l'icône <i class="fa fa-search" aria-hidden="true"></i> en bas
  * commencer à taper le nom de l'application (par exemple `dav` pour **davx⁵**)
  * appuyer sur l'application
  * cliquer sur le bouton **Installer**

## Mise à jour des applications

Pour mettre à jour vos applications sur le magasin F-droid vous devez&nbsp;:

  1. *tirer* vers le bas la page d'accueil de l'application
  * attendre que la synchronisation se fasse (un message **Mise à jour des dépôts** s'affichera en haut)
  * cliquer sur <i class="fa fa-bell" aria-hidden="true"></i> **Mise à jour** (en bas)
  * cliquer sur le bouton **Mettre à jour** en face des applications
  * cliquer sur **Installer** pour installer la mise à jour
