# Prise en mains

## Partage de notes

Pour partager des notes avec d'autres personnes vous devez créer un **persona**. Si vous ne l'avez pas fait à la création de votre compte, vous devez&nbsp;:

  * cliquer sur <i class="fa fa-bars" aria-hidden="true"></i> pour accéder à vos paramètres
  * cliquer sur **Persona public**
  * cliquer sur <i class="fa fa-plus-circle" aria-hidden="true"></i> (en bas à droite)
  * entrer une adresse mail et un nom (le nom est facultatif)

Une clé GPG est alors générée. Vous avez désormais la possibilité de partager des tableaux avec un autre compte persona. Pour cela vous devez&nbsp;:

  * aller dans l'onglet **Tableaux** et cliquer sur l'icône <i class="fa fa-ellipsis-v" aria-hidden="true"></i> puis sur **Partager ce tableau**
  ![gif partage tableau](images/turtl-partage.gif)
  * indiquer l'adresse persona avec laquelle vous souhaitez partager dans **Adresse email de l’invité·e**
  * optionnellement : vous pouvez protéger l'invitation en cochant **Protéger cette invitation** et en saisissant le mot de passe que le compte persona avec lequel vous partagez devra entrer pour voir le partage.

La personne avec qui vous partagez recevra l'invitation dans l'onglet **Partage**.

![image invitation](images/turtl_invit.png)

Il lui faudra alors cliquer sur <i class="fa fa-ellipsis-v" aria-hidden="true"></i> pour accepter l'invitation.

Si l'icône <i class="fa fa-lock" aria-hidden="true"></i> est présente, il faudra entrer le mot de passe (que la personne faisant le partage devra partager) pour déverrouiller **et** accepter.

Si vous êtes la personne qui partage le tableau, vous pouvez à tout moment l'arrêter en allant dans l'onglet **Tableaux**, puis en cliquant sur <i class="fa fa-ellipsis-v" aria-hidden="true"></i> et enfin sur **Ne plus partager**.

Si vous êtes la personne avec qui quelqu'un partage un tableau, vous pouvez à tout moment l'arrêter en allant dans l'onglet **Tableaux**, puis en cliquant sur <i class="fa fa-ellipsis-v" aria-hidden="true"></i> et enfin sur **Quitter ce tableau**.
