# MyFrama

<div class="alert-warning alert">
<b class="violet">My</b><b class="vert">Frama</b> a fermé ses portes le <a href="https://framablog.org/2020/03/03/10-bonnes-raisons-de-fermer-certains-services-framasoft-la-5e-est-un-peu-bizarre/">mardi 12 janvier 2021</a>. Vous trouverez un service similaire <a href="https://alt.framasoft.org/fr/myframa">sur cette page</a>.<br>
</div>

[MyFrama](https://my.framasoft.org) est un service en ligne libre qui permet de conserver, trier, synchroniser et retrouver ses adresses web favorites, dont celles des services Framasoft que vous utilisez.

Il est propulsé par le logiciel libre [Shaarli](https://github.com/shaarli/Shaarli/).

![Exemple d’un compte Framanotes](images/myframa-complet.png)

Tutoriel pour bien débuter&nbsp;: [MyFrama, exemple d’utilisation](exemple-d-utilisation.html)

### Pour aller plus loin&nbsp;:

- [Déframasoftiser Internet](deframasoftiser.html)
- ~~Essayer [MyFrama](https://my.framasoft.org)~~
- [Prise en main](prise-en-main.html)
- Application Android
    - Sur le [store libre
        FDroid](https://f-droid.org/repository/browse/?%20fdid=com.dimtion.shaarlier)
    - Sur le [Google Play
        Store](https://play.google.com/store/apps/details?id=com.dimtion.shaarlier&hl=fr)
- [Participer au code](https://github.com/shaarli/Shaarli/) de Shaarli
- Installer [Shaarli sur vos serveurs](http://framacloud.org/cultiver-son-jardin/installation-de-shaarli/)
- [Dégooglisons Internet](https://degooglisons-internet.org/)
- [Soutenir Framasoft](https://soutenir.framasoft.org/)
