# Framasite

<div class="alert-warning alert">
<b>Déframasoftisons Internet</b> : l’association Framasoft a programmé la fermeture de certains services, pour éviter d’épuiser nos maigres ressources et pour favoriser l’utilisation de petits hébergeurs de proximité (<a href="https://framablog.org/2020/03/03/10-bonnes-raisons-de-fermer-certains-services-framasoft-la-5e-est-un-peu-bizarre/">toutes les infos ici</a>).
Framasite fermera ses portes le <b>mardi 6 juillet 2021</b>.
Nous avons réuni des outils pour vous aider à récupérer vos données et à trouver un service similaire <a href="https://alt.framasoft.org/fr/framasite">sur cette page</a>.
</div>

[Framasite](https://frama.site/) est un service libre de création de sites web.

1. Créez votre site
2. Connectez-vous à son interface de gestion
3. Personnalisez et publiez vos pages web

![images framasites](images/site-diaporama-creation02.png)

## Pour aller plus loin&nbsp;:

- Utiliser [Framasite](https://frama.site/)
- [Déframasoftiser Internet](deframasoftiser.html)
- [Prise en main](prise-en-main.html)
- [Composants de base](composants-de-base.html)
- [Composants bootstrap](composants-bootstrap.html)
- [Modules](modules.html)
- [Markdown](markdown.html)
- [Site de démonstration](https://demo-blog.frama.site/)
- Un service proposé dans le cadre de la campagne [contributopia](https://contributopia.org/)
