# Rattacher son nom de domaine à un site ou wiki

<div class="alert alert-danger">
    Attention, cette manipulation est à réserver aux utilisatrices et utilisateurs avancé·e·s&nbsp;!
</div>

Si vous avez un nom de domaine que vous souhaitez rattacher à un de vos sites ou wiki, c'est possible. Votre adresse en ".frama.site" ou ".frama.wiki" restera disponible, mais c'est votre nom de domaine qui deviendra l'adresse principale.


## C'est quoi ?

Cette étape vous permet de rattacher un nom de domaine existant (par exemple "champignons-quercy.fr", "boulangeriedu92.org") à un de vos sites hébergés par framasite (par exemple "champiquercy.frama.wiki" ou "boulange92.frama.site").

Ainsi, quand quelqu'un tapera https://champignons-quercy.fr dans son navigateur, c'est le contenu de https://champiquercy.frama.site qui s'affichera.

<div class="alert alert-info">
  Notes :
  <ol>
      <li>il ne s'agit pas d'une simple redirection, si vous avez une page <a href="https://champiquercy.frama.site/notre-association.html">https://champiquercy.frama.site/notre-association.html</a> la page <a href="https://champignons-quercy.fr/notre-association.html">https://champignons-quercy.fr/notre-association.html</a> existera aussi (c'est à dire que vos visiteurs, une fois arrivés sur <a href="https://champignons-quercy.fr">https://champignons-quercy.fr</a> ne verront pas qu'en fait il s'agit du site https://champiquercy.frama.site</li>
      <li>le site restera accessible via <a href="https://champiquercy.frama.site">https://champiquercy.frama.site</a>, même après avoir rattaché votre site. Cela afin de palier à d'éventuels problèmes (ex: expiration du nom de domaine, référencement, dépannage,etc)</li>
      <li>le certificat numérique de votre site (c'est à dire le fait que les communications entre le navigateur de vos visiteurs et le serveur Framasite soient protégées) est généré automatiquement.</li>
  </ol>
</div>

<div class="alert alert-warning">Dans quelques semaines, cette étape pourra être réalisée directement depuis le site Framasite, en déléguant à Framasoft l'achat du domaine et son rattachement automatique à votre Framasite. D'ici là, la procédure doit être faite manuellement.</div>

## Comment ça marche&nbsp;?

### Étape 1 : créer un Framasite

D'abord, il vous faut que le Framasite que vous voulez attacher à un nom de domaine soit déjà existant. Logiquement, si vous êtes sur cette page, c'est que cela doit être déjà le cas :)

### Étape 2 : Réserver son nom de domaine

<div class="alert alert-info">NB : si vous avez déjà réservé votre nom de domaine, vous pouvez directement passer à l'étape 5.</div>

Ensuite, il vous faut acheter le nom de domaine de votre choix chez une entreprise spécialisée, qu'on appelle <em>«&nbsp;registrar&nbsp;»</em>.

Plutôt que d'achat, nous devrions parler de location, car dans les faits, les domaines sont valables pour une ou plusieurs années, pendant lesquelles vous serez bien identifié⋅e comme en étant le ou la propriétaire. Mais à la date d'expiration de ce domaine (par exemple un an après l'avoir réservé), si vous ne renouvelez pas cette réservation, votre domaine sera à nouveau disponible pour le premier ou la première venue.

Concrètement, réserver le domaine "<a href="https://champignons-quercy.fr">champignons-quercy.fr</a>" vous coûtera environ 15€ par an. Ce prix varie selon les <em>registrars</em> et les «&nbsp;extensions&nbsp;» (le "<a href="https://champignons-quercy.fr">champignons-quercy.fr</a>" peut coûter moins cher que "<a href="https://champignon-quercy.paris">champignon-quercy.paris</a>", par exemple). Et il vous appartient de renouveler cette réservation chaque année.

Il existe <a href="https://www.icann.org/registrar-reports/accredited-list.html">des milliers de <em>registrars</em></a>, mais nous allons suivre la procédure chez un <em>registrar</em> français bien connu pour son éthique : <a href="https://gandi.net">Gandi.net</a>.

### Étape 3 : vérifier la disponibilité de son nom de domaine

Sur le site de Gandi, saisissez le nom de domaine qui vous intéresse&nbsp;:

<img src="images/4rDWsviH2OqG.png">

Le domaine est disponible ? Parfait ! Ajoutez-le à votre panier&nbsp;:
<img src="images/Ti8D25a6cQKk.png">

Le domaine est déjà pris ? Dommage, nous ne pouvons rien pour vous : il faudra vous trouver un autre nom de domaine et répéter cette étape autant de fois que nécessaire...

### Étape 4 : payer le nom de domaine

Si vous avez un compte, identifiez-vous. Sinon, créez vous un nouveau compte et laissez-vous guider&nbsp;:

<img src="images/Fp3ywCoIBTFk.png">

La procédure peut prendre quelques heures, vous recevrez un email du registrar une fois le domaine correctement réservé.

### Étape 5 : modifier les DNS

C'est l'étape la plus technique.

Elle consiste à modifier la [zone DNS](https://fr.wikipedia.org/wiki/Domain_Name_System) de votre domaine (par exemple "https://champignons-quercy.fr/") pour que ce dernier pointe vers la machine qui héberge votre framasite. Ainsi, quand quelqu'un tapera " https://champignons-quercy.fr", c'est le contenu de votre framasite qui s'affichera.

Techniquement, il suffit de faire pointer les champs `A` et `AAAA` sur les valeurs `144.76.131.210` et `2a01:4f8:141:3421::210`.

Vous n'avez rien compris à la phrase précédente ? Pas de souci, voici un exemple clair et simple à comprendre.

Prêt⋅e&nbsp;? C'est parti&nbsp;!

  1. Sélectionnez votre domaine dans la liste de vos domaines
  <img src="images/OZQF4DapmZk9.png">
  - après avoir cliqué sur le domaine, cliquez sur "Créer une copie"
  <img src="images/cSMVpYbtAJbB.png">
  - Après avoir validé l'écran intermédiaire, cliquez sur "Éditez la zone"
  <img src="images/7mODN5w2BcGz.png">
  - Vérifiez que vous êtes en mode "Normal", puis cliquez sur "Créez une nouvelle version"
  <img src="images/jMZze1seQkYx.png">
  - Cliquez sur le bouton permettant d'éditer la première ligne (Nom `@`, type `A`)
  <img src="images/W5vv5LWjlKc6.png">
  - Repérez le champ "valeur" qui sera remplacé
  <img src="images/3pwYlTEefoJc.png">
  - Saisissez `144.76.131.210` comme valeur, puis validez
  <img src="images/MZ2JjC00UBlj.png">
  - De retour sur l'écran précédent, cliquez sur "Ajout" en bas de page
  <img src="images/HcVbD8Uy7hYF.png">
  - Ajoutez un champ de type `AAAA`, de nom `@` et de valeur `2a01:4f8:141:3421::210`, puis validez
  <img src="images/bU1trHhKptkU.png">
  - Enfin, étape importante, cliquez bien sur "Activez cette version"
  <img src="images/ECmXUBH0Ab3H.png">

<div class="alert alert-info">Pour attacher un sous-domaine <code>www</code> vous devez ajouter&nbsp;:

<pre>
www 10800 IN A 144.76.131.210
www 1800 IN AAAA 2a01:4f8:141:3421::210
</pre>

et ajouter <code>www.“votredomaine”.fr</code> dans l’interface de gestion de votre site sur frama.site (en remplaçant « votredomaine.fr » par votre nom de domaine).
</div>

  Voilà ! Le plus dur est fait :) Mais ça n'est pas fini :-/
  - Attendez quelques heures. Ce temps est nécessaire pour la mise à jour des informations. Comptez en gros en 3H et 24H.
  - Sur frama.site, allez sur la page <a class="btn btn-default" href="https://frama.site/domain/attach" target="_blank" rel="noopener noreferrer">Rattacher un domaine</a>
