# Framalink / Huitre

<div class="alert-warning alert">
Dans le cadre de la campagne Déframasoftisons Internet, Framalink est en cours de fermeture.
<b>Depuis début 2021, il n’est plus possible de raccourcir de nouveau lien sur le service. Les liens raccourcis avant cette date restent valides.</b>
Trouver des alternatives : https://alt.framasoft.org/framalink/.
</div>

[Frama.link](https://frama.link) ou [Huit.re](https://huit.re) est un service en ligne libre et minimaliste qui permet de raccourcir des liens.

  1. Collez le lien à raccourcir dans le formulaire.
  2. Si besoin, choisissez le texte du lien raccourci.
  3. Partagez ensuite avec vos correspondants le lien qui vous est donné.

Le service repose sur le logiciel libre [Lstu](https://lstu.fr/).

---

## Tutoriel vidéo

<div class="text-center">
  <p><video controls="controls" preload="none" poster="https://framatube.org/images/media/990l.jpg" height="340" width="570">
    <source src="https://framatube.org/blip/framalink.mp4" type="video/mp4">
    <source src="https://framatube.org/blip/framalink.webm" type="video/webm">
  </video></p>
  <p>→ La <a href="https://framatube.org/blip/framalink.webm">vidéo au format webm</a></p>
</div>

Vidéo réalisée par [arpinux](http://arpinux.org/), artisan paysagiste de la distribution GNU/Linux pour débutant [HandyLinux](https://handylinux.org/)

## Pour aller plus loin&nbsp;:

  * [Déframasoftiser Internet](deframasoftiser.html)
  * [Essayer Frama.link](https://frama.link) ou [Huit.re](https://huit.re)
  * [Les fonctionnalités](fonctionnalites.md)
