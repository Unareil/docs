# Modération sur discourse (Framacolibri)

Sous chaque message, vous pouvez retrouver une icône de drapeau <i class="fa fa-flag" aria-hidden="true"></i> signifiant “Signaler secrètement ce message pour attirer l’attention ou envoyer une notification à son propos”

![image montrant comment signaler sous un message](images/discourse_modration_signalement.png)

Cette icône <i class="fa fa-flag" aria-hidden="true"></i> se trouve aussi tout en bas de chaque sujet.

![image montrant comment l'icône signaler sous un sujet](images/discourse_modration_signalement_topic.png)

Une fois cliqué sur cette icône drapeau <i class="fa fa-flag" aria-hidden="true"></i> vous aurez accès à une pop-up vous permettant de prédéfinir de quel type de contenu inapproprié il s’agit : Hors-sujet, contenu inapproprié selon <a href="https://framasoft.org/fr/moderation/" title="lien vers la charte de modération">la charte</a>, spam, ou autre (en sélectionnant Autre vous pourrez détailler le pourquoi du signalement).

![image montrant la fenêtre de signalements](images/discourse_modration_signalement_explications.png)

Votre signalement sera ensuite envoyé à l’équipe de modération bénévole.
