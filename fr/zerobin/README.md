# Framabin

[Framabin](https://framabin.org) est un service en ligne libre et minimaliste qui permet de partager des textes de manière confidentielle et sécurisée.

  1. Collez le texte à transmettre.
  2. Si besoin, définissez sa durée de conservation en ligne, ajoutez un espace de discussion ou activez la coloration du code.
  3. Partagez ensuite avec vos correspondants le lien qui vous est donné.

Vos données (commentaires inclus) sont **chiffrées dans le navigateur web** en utilisant l’[algorithme AES 256 bits](http://fr.wikipedia.org/wiki/Advanced_Encryption_Standard).
Elles sont ensuite transmises et stockées sur nos serveurs sans qu’il nous soit possible de les déchiffrer. **Vous seuls possédez la clé** utilisée pour chiffrer et déchiffrer les données.

Le service repose sur le logiciel libre [Zerobin](http://sebsauvage.net/wiki/doku.php?id=php:zerobin).

---

## Tutoriel vidéo

<div class="text-center">
  <p><video controls="controls" preload="none" poster="https://framatube.org/images/media/991l.jpg" height="340" width="570">
      <source src="https://framatube.org/blip/framabin.mp4" type="video/mp4">
      <source src="https://framatube.org/blip/framabin.webm" type="video/webm">
  </video></p>
  <p>→ La <a href="https://framatube.org/blip/framabin.webm">vidéo au format webm</a></p>
</div>

Vidéo réalisée par [arpinux](http://arpinux.org/), artisan paysagiste de la distribution GNU/Linux pour débutant [HandyLinux](https://handylinux.org/)
