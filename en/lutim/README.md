# Framapic

<div class="alert alert-warning">
<b class="violet">Frama</b><b class="vert">pic</b> closed on <a href="https://framablog.org/2019/09/26/lets-de-frama-tify-the-internet/">12 January 2021</a>. You will find a similar service <a href="https://alt.framasoft.org/en/framapic/">on this page.</a>.<br>
</div>

[Framapic](https://framapic.org) is a free and open source service which allows you to share pictures in a secure and confidential way.

  1. Paste the image you want to share.
  2. If needed, define the retention policy.
  3. You can then share the link you are given with other people for them to see the picture.


Your pictures are encrypted and stored on our servers. We cannot see the content of your files nor decrypt them.
This service is based on a free software: [Lutim](https://lut.im/).

Thanks to a new feature added by its developer, [Framapic](https://framapic.org) now allows you to easily create an image gallery! Find out how in our [example](gallery.md).

## Video Tutorial

<div class="text-center">
  <p><video controls="controls" preload="none" poster="https://framatube.org/images/media/989l.jpg" height="340" width="570">
      <source src="https://framatube.org/blip/framapic.mp4" type="video/mp4">
      <source src="https://framatube.org/blip/framapic.webm" type="video/webm">
  </video></p>
  <p>→<a href="https://framatube.org/blip/framapic.webm">webm format</a></p>
</div>

Tutorial made by [arpinux](http://arpinux.org/), *landscape architect* of GNU/Linux beginners linux distribution [HandyLinux](https://handylinux.org/).

---

## See more:

  * ~~[Try Framapic](https://framapic.org)~~
  * [How to](how-to.md)
  * [Create a gallery](gallery.md)
  * Android app:
    * [Goblim](https://f-droid.org/packages/fr.mobdev.goblim/) on F-droid
    * [Goblim](https://play.google.com/store/apps/details?id=fr.mobdev.goblim) on Google Play
  * [De-google-ify Internet](https://degooglisons-internet.org/en)
  * [Support Framasoft](https://soutenir.framasoft.org)
