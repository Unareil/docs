# How to

## How to modify expiration date?

To modify an expiration date you have to, **with the same browser used to upload**, go to **My images** and:

  1. click <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
  * change the date
  * click **Save changes**

![image showing how to edit expiration date](img/lutim_edit_date_en.png)
